﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;
using WebExperience.DataLayer;

namespace WebExperience.ViewModelLayer
{
    public class AssetViewModel
    {
        public List<Asset> assetCollection { get; set; }
        public List<Country> countryCollection { get; set; }
        public List<Mime_Type> mimeCollection { get; set; }
        public string Message {get; set;}

        public void init()
        {
            assetCollection = new List<Asset>();
            countryCollection = new List<Country>();
            mimeCollection = new List<Mime_Type>();
            Message = string.Empty;

        }

        public AssetViewModel():base()
        {
            init();
        }
        public Asset GetAssetDetailsById(string assetId)
        {
            Asset asset = new Asset();
            asset = assetCollection.Find(x => x.asset_id == assetId);
            return asset;
        }
        public string GetCountryNameById(int id)
        {
            Country country = countryCollection.Find(x => x.Country_Id == id);
            return country.Country_name;

        }
        public string GetMimeTypeById(int id)
        {
            Mime_Type mimeType = mimeCollection.Find(x => x.mimetype_id == id);
                return mimeType.type;
        }
        public void HandleRequest()
        {
            BuildCollection();
        }
        protected void BuildCollection()
        {
            AssetData assDt = null;
            try
            {
                assDt = new AssetData();
                assetCollection = assDt.Assets.ToList();
                countryCollection = assDt.Countries.ToList();
                mimeCollection = assDt.Mime_Type.ToList();

            }
            catch (Exception e)
            {
              //
                
                //Publish()
            }
        }
    }
}
