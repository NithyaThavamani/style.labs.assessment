namespace WebExperience.DataLayer
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class AssetData : DbContext
    {
        public AssetData()
            : base("name=AssetData")
        {
        }

        public virtual DbSet<Asset> Assets { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<Mime_Type> Mime_Type { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
