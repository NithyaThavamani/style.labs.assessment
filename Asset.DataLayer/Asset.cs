namespace WebExperience.DataLayer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Asset
    {
        [Key]
        [DisplayName("ASSETID")]
        public string asset_id{ get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("FILE NAME")]
        public string file_name { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("CREATED PERSON")]
        public string created_by { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [DisplayName("EMAIL ID")]
        public string email { get; set; }

        [Required]
        [DisplayName("COUNTRY NAME")]
        public int Country_Id { get; set; }

        [Required]
        [DisplayName("MIME TYPE")]
        public int mimetype_id { get; set; }
        [Required]
        [StringLength(500)]
        [DisplayName("DESCRIPTION")]
        public string description { get; set; }

        public virtual Country Country { get; set; }

        public virtual Mime_Type Mime_Type { get; set; }
    }
}
