﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml;
using System.Collections;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// This test evaluates the 
    /// </summary>
    public class XmlReadingTest : ITest
    {
        public string Name { get { return "XML Reading Test"; } }

        ArrayList arrList = new ArrayList();
        string[] arrTemp = new string[4];
        string[] arrPH = new string[4];
        string[] arrChloride = new string[4];
        string[] arrPhospate = new string[4];
        string[] arrNitrate = new string[4];

        float totTemp = 0;
        float totPH = 0;
        float totChloride = 0;
        float totPhospate = 0;
        float totNitrate = 0;

        float minTemp = 0;
        float maxTemp = 0;
        float minPH = 0;
        float maxPH = 0;
        float minChloride = 0;
        float maxChloride = 0;
        float minPhospate = 0;
        float maxPhospate = 0;
        float minNitrate = 0;
        float maxNitrate = 0;

        int countTemp = 0;
        int countPH = 0;
        int countChloride = 0;
        int countPhospate = 0;
        int countNitrate = 0;

        public void Run()
        {
            
            XDocument xdoc = XDocument.Load(@"..\\..\\Resources\\SamplePoints.xml");
            var parentMeasurement = xdoc.Descendants("measurement");
            foreach (var childMeasurement in parentMeasurement)
            {
                var childParam = childMeasurement.Elements();
                foreach (XElement child in childParam)
                {
                    float[] arr = new float[3];
                    XAttribute attr = child.Attribute("name");
                    var name = attr.Value;
                    float value = float.Parse(child.Value);
                    switch (name)
                    {
                        case "temperature":
                            countTemp++;
                            arr = parameterCalculation(value, minTemp, maxTemp, totTemp);
                            minTemp = arr[0];
                            maxTemp = arr[1];
                            totTemp = arr[2];
                            break;
                        case "pH":
                            countPH++;
                            arr = parameterCalculation(value, minPH, maxPH, totPH);
                            minPH = arr[0];
                            maxPH = arr[1];
                            totPH = arr[2];
                            break;
                        case "Chloride":
                            countChloride++;
                            arr = parameterCalculation(value, minChloride, maxChloride, totChloride);
                            minChloride = arr[0];
                            maxChloride = arr[1];
                            totChloride = arr[2];
                            break;
                        case "Phosphate":
                            countPhospate++;
                            arr = parameterCalculation(value, minPhospate, maxPhospate, totPhospate);
                            minPhospate = arr[0];
                            maxPhospate = arr[1];
                            totPhospate = arr[2];
                            break;
                        case "Nitrate":
                            countNitrate++;
                            arr = parameterCalculation(value, minNitrate, maxNitrate, totNitrate);
                            minNitrate = arr[0];
                            maxNitrate = arr[1];
                            totNitrate = arr[2];
                            break;
                    }
                }
            }

            AddValuesToList();
            PrintOverview(arrList);

        }

        private void PrintOverview(IEnumerable arrList)
        {
            Console.WriteLine(Name);
            Console.WriteLine("--------------------------------------");
            Console.WriteLine(string.Format("{0,-15}    {1,-10}     {2,-10}    {3,-10}", "PARAMETER", "LOW", "AVG", "MAX"));
            foreach (Object[] obj in arrList)
            {
                Console.WriteLine(string.Format("{0,-15}    {1,-10}     {2,-10}    {3,-10}", obj[0], obj[1], obj[2], obj[3]));

            }

        }

        private float[] parameterCalculation(float value, float minParam, float maxParam, float totParam)
        {
            float[] arr = new float[3];
            if (minParam == 0 || maxParam == 0 || totParam == 0)
            {
                minParam = value;
                maxParam = value;
                totParam = value;

            }
            else
            {
                if (value < minParam)
                {
                    minParam = value;

                }
                if (value > maxParam)
                {
                    maxParam = value;

                }
                totParam = totParam + value;
            }
            arr[0] = minParam;
            arr[1] = maxParam;
            arr[2] = totParam;
            return arr;
        }
        private void AddValuesToList()
        {
            arrTemp[0] = "Temperature";
            arrTemp[1] = minTemp.ToString();
            arrTemp[2] = (totTemp / countTemp).ToString();
            arrTemp[3] = maxTemp.ToString();

            arrPH[0] = "PH";
            arrPH[1] = minPH.ToString();
            arrPH[2] = (totPH / countPH).ToString();
            arrPH[3] = maxPH.ToString();

            arrChloride[0] = "Chloride";
            arrChloride[1] = minChloride.ToString();
            arrChloride[2] = (totChloride / countChloride).ToString();
            arrChloride[3] = maxChloride.ToString();

            arrPhospate[0] = "Phospate";
            arrPhospate[1] = minPhospate.ToString();
            arrPhospate[2] = (totPhospate / countPhospate).ToString();
            arrPhospate[3] = maxPhospate.ToString();

            arrNitrate[0] = "NITRATE";
            arrNitrate[1] = minNitrate.ToString();
            arrNitrate[2] = (totNitrate / countNitrate).ToString();
            arrNitrate[3] = maxNitrate.ToString();
            
            arrList.Add(arrTemp);
            arrList.Add(arrPH);
            arrList.Add(arrChloride);
            arrList.Add(arrPhospate);
            arrList.Add(arrNitrate);
        }
    }
}
