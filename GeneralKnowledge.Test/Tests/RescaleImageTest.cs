﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Configuration;
namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Image rescaling
    /// </summary>
    public class RescaleImageTest : ITest
    {
        public void Run()
        {
            // TODO:
            // Grab an image from a public URL and write a function thats rescale the image to a desired format
            // The use of 3rd party plugins is permitted
            // For example: 100x80 (thumbnail) and 1200x1600 (preview)
            try
            {
                Console.WriteLine();
                Console.WriteLine("RESCALE IMAGE TEST");
                Console.WriteLine("-------------------");
                Image imgPhoto = Image.FromFile(ConfigurationManager.AppSettings["imageSrcPath"]);
                Image image = ResizeImage(imgPhoto, 800, 1000);
                image.Save(ConfigurationManager.AppSettings["resizeImgPath"]);
                Console.WriteLine("Resized Image saved at" + ConfigurationManager.AppSettings["resizeImgPath"]);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public Image ResizeImage(Image img, int width, int height)
        {
            Bitmap b = new Bitmap(width, height);
            using (Graphics g = Graphics.FromImage((Image)b))
            {
                g.DrawImage(img, 10, 10, width, height);
            }
            return (Image)b;
        }
    }
}
