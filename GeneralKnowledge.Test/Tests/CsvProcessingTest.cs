﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using System.IO;
using System.Reflection;
using EntityFramework.Seeder;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// CSV processing test
    /// </summary>
    public class CsvProcessingTest : ITest
    {
        public void Run()
        {
            Console.WriteLine();
            Console.WriteLine("CSV PROCESSING TEST");
            Console.WriteLine("-------------------------------");
            try
            {
                using (var context = new AssetContext())
                {
                    List<Country> countryList = new List<Country>();
                    List<Mime_Type> mimetypeList = new List<Mime_Type>();
                    List<Asset> assetList = new List<Asset>();
                    Assembly assembly = Assembly.GetExecutingAssembly();
                    string resourceName = "GeneralKnowledge.Test.App.Resources.AssetImport.csv";
                    using (Stream stream = assembly.GetManifestResourceStream(resourceName))
                    {
                        using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                        {
                            List<string> countryNames = new List<string>();
                            List<string> mimeTypes = new List<string>();
                            int cCount = 0;
                            int mCount = 0;
                            CsvReader csvReader = new CsvReader(reader);
                            while (csvReader.Read())
                            {
                                if (csvReader.GetField(0) != "asset_id")
                                {
                                    Asset asset = new Asset();
                                    asset.asset_id = csvReader.GetField(0);
                                    asset.created_by = csvReader.GetField(3);
                                    asset.description = csvReader.GetField(6);
                                    asset.email = csvReader.GetField(4);
                                    asset.file_name = csvReader.GetField(1);
                                    string countryName = csvReader.GetField(5);
                                    if (!countryNames.Contains(countryName))
                                    {
                                        Country country = new Country();
                                        country.Country_Id = ++cCount;
                                        country.Country_name = countryName;
                                        countryList.Add(country);
                                        countryNames.Add(countryName);
                                        asset.Country_Id = country.Country_Id;

                                    }
                                    else
                                    {

                                        var con = countryList.Find(x => x.Country_name == countryName);
                                        asset.Country_Id = con.Country_Id;
                                    }
                                    string mimeType = csvReader.GetField(2);
                                    if (!mimeTypes.Contains(mimeType))
                                    {
                                        Mime_Type mime_type = new Mime_Type();
                                        mime_type.mimetype_id = ++mCount;
                                        mime_type.type = mimeType;
                                        mimetypeList.Add(mime_type);
                                        mimeTypes.Add(mimeType);
                                        asset.mimetype_id = mime_type.mimetype_id;
                                    }
                                    else
                                    {
                                        var mime = mimetypeList.Find(x => x.type == mimeType);
                                        asset.mimetype_id = mime.mimetype_id;
                                    }
                                    assetList.Add(asset);
                                }
                            }
                        }
                    }
                    context.Database.Initialize(force: true);
                    Console.WriteLine("Inserting Country Table...");
                    context.BulkInsert(countryList);
                    Console.WriteLine("Inserting MimeType Table...");
                    context.BulkInsert(mimetypeList);
                    Console.WriteLine("Inserting Assets Table...");
                    context.BulkInsert(assetList, bulk => bulk.BatchSize = 50000);                    
                    context.BulkSaveChanges(bulk => bulk.BatchSize = 50000);
                    Console.WriteLine("Database Created Successfully and DB Name =  " +context.Database.Connection.Database);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}





