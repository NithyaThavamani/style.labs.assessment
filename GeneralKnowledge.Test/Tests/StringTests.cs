﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Basic string manipulation exercises
    /// </summary>
    public class StringTests : ITest
    {
        public void Run()
        {
            // TODO:
            // Complete the methods below

            AnagramTest();
            GetUniqueCharsAndCount();
        }

        private void AnagramTest()
        {
            Console.WriteLine("POSSIBLE ANAGRAMS");
            Console.WriteLine("-------------------------------");
            var word = "stop";
            var possibleAnagrams = new string[] { "test", "tops", "spin", "post", "mist", "step" };

            foreach (var possibleAnagram in possibleAnagrams)
            {
                Console.WriteLine(string.Format("{0} > {1}: {2}", word, possibleAnagram, possibleAnagram.IsAnagram(word)));
            }
            Console.WriteLine();
        }

        private void GetUniqueCharsAndCount()
        {
            var word = "xxzwxzyzzyxwxzyxyzyxzyxzyzyxzzz";            
            Console.WriteLine("UNIQUE CHARACTERS AND ITS COUNT");
            Console.WriteLine("-------------------------------");
            Console.WriteLine("GIVEN STRING IS  " + word);
            Console.WriteLine();
            var wordUnique = new string(word.Distinct().ToArray());
            foreach (char cUnique in wordUnique)
            {
                int wordCount = 0;
                foreach (char cWord in word)
                {
                    if (cUnique == cWord)
                    {
                        wordCount++;
                    }
                }
                Console.WriteLine("Char : " + cUnique + "   Count : " + wordCount);
            }
            Console.WriteLine();
        }
    }

    public static class StringExtensions
    {
        public static bool IsAnagram(this string a, string b)
        {
            // TODO: 

            // Sort the strings in an ascending order 
            var strAnagram = string.Concat(a.OrderBy(c => c));
            var strWord = string.Concat(b.OrderBy(c => c));

            //Compare the two strings whether they are equal or not
            if (strAnagram == strWord)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
