﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralKnowledge.Test.App
{
    public class Asset
    {
        
        [Index(IsUnique = true)]
        [Key]
        public string  asset_id { get; set; }
        public string file_name { get; set; }
        public string created_by { get; set; }
        public string email { get; set; }       
        public int Country_Id { get; set; }
        public Country country { get; set; }       
        public int mimetype_id { get; set; }
        public Mime_Type mime_type { get; set; }
       public string description { get; set; }
          }
}
