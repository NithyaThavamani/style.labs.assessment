﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Common;

namespace GeneralKnowledge.Test.App
{
    public class AssetContext : DbContext
    {
        public AssetContext() : base("AssetDB")
        {
          //Database.SetInitializer<AssetContext>(new CreateDatabaseIfNotExists<AssetContext>());
            Database.SetInitializer<AssetContext>(new AssetDBInitializer());
            }
       public DbSet<Asset> Asset { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Mime_Type> Mime_Types { get; set; }
    }
}
