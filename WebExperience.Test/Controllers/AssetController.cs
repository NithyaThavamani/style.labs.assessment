﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using WebExperience.ViewModelLayer;
using PagedList;

using WebExperience.DataLayer;

namespace WebExperience.Test.Controllers
{

    public class AssetController : Controller
    {
        AssetData db = new AssetData();

        /*To display all the asset details*/
        public ActionResult Asset(int? page)
        {
            IPagedList<Asset> ass = null;
            try
            {
                int pageSize = 15;
                int pageIndex = 1;
                pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;                
                AssetViewModel assVM = new AssetViewModel();
                assVM.HandleRequest();
                ass = db.Assets.OrderBy(a => a.asset_id).ToPagedList(pageIndex, pageSize);                
            }
            catch (Exception)
            {
                ModelState.AddModelError(string.Empty, "Unable to load assets");
            }
            return View(ass);
        }

        /* To Get asset details for particular assetid */
        [System.Web.Mvc.HttpGet]
        public ActionResult AssetDetails(string id)
        {
            try
            {
                string assetId = Convert.ToString(id);
                AssetViewModel assVM = new AssetViewModel();
                assVM.HandleRequest();
                Asset asset = assVM.GetAssetDetailsById(assetId);
                String countryName = assVM.GetCountryNameById(asset.Country_Id);
                string mimeType = assVM.GetMimeTypeById(asset.mimetype_id);
                ViewBag.asset = asset;
                ViewBag.country = countryName;
                ViewBag.mime = mimeType;
            }
            catch (Exception)
            {
                ModelState.AddModelError(string.Empty,"Unable to retrieve asset details");
            }

            return View();
        }


        /*To get Asset details for particular assetid and countrylist and mimetype list */
        [System.Web.Mvc.HttpGet]
        public ActionResult Edit(string id)
        {
            Asset asset = new Asset();
            try
            {
                asset = db.Assets.Find(id);
                var countries = db.Countries.ToList();
                var mimeTypes = db.Mime_Type.ToList();

                Country objCountry = new Country();
                objCountry.Countries = GetSelectListItemsCountries(countries, asset.Country_Id);
                ViewData["countriesEnumEdit"] = objCountry.Countries;

                Mime_Type objMimeTYpes = new Mime_Type();
                objMimeTYpes.mimeTypes = GetSelectListItemsMimeTypes(mimeTypes, asset.mimetype_id);
                ViewData["mimeTypesEnum"] = objMimeTYpes.mimeTypes;
            }
            catch (Exception)
            {
                ModelState.AddModelError(string.Empty, "Unable to load Edit Page");
            }
            return View(asset);
        }

        /* Update the asset details into database */
        [System.Web.Mvc.HttpPost, System.Web.Mvc.ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost(string id)
        {
            try
            {
                Asset assetToUpdate = db.Assets.Find(id);
                assetToUpdate.file_name = Request.Form["file_name"];
                assetToUpdate.created_by = Request.Form["created_by"];
                assetToUpdate.email = Request.Form["email"];
                assetToUpdate.description = Request.Form["description"];
                assetToUpdate.Country_Id = Convert.ToInt32(Request.Form["Countries"]);
                assetToUpdate.mimetype_id = Convert.ToInt32(Request.Form["mimeTypes"]);
                db.SaveChanges();
                return Content("<script language='javascript' type='text/javascript'>alert('Asset Record updated Successfully');window.location='/Asset/Asset/';</script>");
            }
            catch (Exception)
            {
                ModelState.AddModelError(string.Empty, "Unable to save changes. Try again..");
            }

            return View();
        }


        /* To get asset data , country name and mimetype for particular assetid */
        [System.Web.Mvc.HttpGet]
        public ActionResult Delete(string id, string country, string mimetype)
        {
            Asset asset = new Asset();
            try
            {
                asset = db.Assets.Find(id);
                ViewBag.country = country;
                ViewBag.mime = mimetype;
                if (asset == null)
                {
                    return HttpNotFound();
                }
            }
            catch (Exception)
            {
                ModelState.AddModelError(string.Empty, "Unable to load delete page");
            }
            return View(asset);
        }

        /*deletes the asset entry from database */
        // POST: Student/Delete/5
        [System.Web.Mvc.HttpPost, System.Web.Mvc.ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeletePost(string id)
        {
            try
            {
                Asset asset = db.Assets.Find(id);
                db.Assets.Remove(asset);
                db.SaveChanges();
                return Content("<script language='javascript' type='text/javascript'>alert('Asset Record deleted Successfully');window.location='/Asset/Asset/';</script>");

            }
            catch (Exception)
            {
                ModelState.AddModelError(string.Empty, "Unable to delete asset data");
            }
            return View();
        }


        /* Gets countries and mimetypes and bind it to Enumeration Datalist for dropdown binding */
        [System.Web.Mvc.HttpGet]
        public ActionResult Create()
        {
            try
            {
                var countries = db.Countries.ToList();
                var mimeTypes = db.Mime_Type.ToList();

                Country objCountry = new Country();
                objCountry.Countries = GetSelectListItemsCountries(countries);
                ViewData["countriesEnum"] = objCountry.Countries;

                Mime_Type objMimeTYpes = new Mime_Type();
                objMimeTYpes.mimeTypes = GetSelectListItemsMimeTypes(mimeTypes);
                ViewData["mimeTypesEnum"] = objMimeTYpes.mimeTypes;
            }
            catch (Exception)
            {
                ModelState.AddModelError(string.Empty, "Unable to load create page");
            }

            return View();
        }


        /*Inserts new asset entry into database */
        [System.Web.Mvc.HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FormCollection formValues)
        {
            Asset objAsset = new Asset();
            try
            {
                if (ModelState.IsValid)
                {
                    Guid obj = Guid.NewGuid();
                    objAsset.asset_id = obj.ToString();
                    objAsset.file_name = Request.Form["file_name"];
                    objAsset.created_by = Request.Form["created_by"];
                    objAsset.email = Request.Form["email"];
                    objAsset.description = Request.Form["description"];
                    objAsset.Country_Id = Convert.ToInt32(Request.Form["Country"]);
                    objAsset.mimetype_id = Convert.ToInt32(Request.Form["MimeType"]);
                    db.Assets.Add(objAsset);
                    db.SaveChanges();
                    return Content("<script language='javascript' type='text/javascript'>alert('New Asset Record Inserted Successfully');window.location='/Asset/Asset/';</script>");
                    
                }
            }
            catch (Exception)
            {
               ModelState.AddModelError(string.Empty, "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            return View();
        }


        /*Binds the countrylist into List<SelectListItem> */
        private IEnumerable<SelectListItem> GetSelectListItemsCountries(IEnumerable<Country> countries)
        {
            var selectList = new List<SelectListItem>();
            foreach (var country in countries)
            {
                selectList.Add(new SelectListItem
                {
                    Value = country.Country_Id.ToString(),
                    Text = country.Country_name
                });
            }
            return selectList;
        }

        /* Binds the mimetype lists into list<selectedListItem> */
        private IEnumerable<SelectListItem> GetSelectListItemsMimeTypes(IEnumerable<Mime_Type> mimeTypes)
        {
            var selectList = new List<SelectListItem>();
            foreach (var mimeType in mimeTypes)
            {
                selectList.Add(new SelectListItem
                {
                    Value = mimeType.mimetype_id.ToString(),
                    Text = mimeType.type
                });
            }
            return selectList;
        }

        /* Binds the countrylist into list<selectedListItem> and sets the selected property based on the countryid */
        private IEnumerable<SelectListItem> GetSelectListItemsCountries(IEnumerable<Country> countries, int id)
        {
            var selectList = new List<SelectListItem>();
            foreach (var country in countries)
            {
                selectList.Add(new SelectListItem
                {
                    Value = country.Country_Id.ToString(),
                    Text = country.Country_name,

                    Selected = country.Country_Id == id ? true : false,

                });
            }
            return selectList;
        }


        /* Binds the mimetype lists into list<selectedListItem> and sets the selected property based on the mimetypeid */
        private IEnumerable<SelectListItem> GetSelectListItemsMimeTypes(IEnumerable<Mime_Type> mimeTypes, int id)
        {
            var selectList = new List<SelectListItem>();
            foreach (var mimeType in mimeTypes)
            {
                selectList.Add(new SelectListItem
                {
                    Value = mimeType.mimetype_id.ToString(),
                    Text = mimeType.type,
                    Selected = mimeType.mimetype_id == id ? true : false
                });
            }
            return selectList;
        }
    }
}

